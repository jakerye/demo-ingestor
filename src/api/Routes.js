const log = require( '@leverege/log' )
const Err = require( '@leverege/error' )

function install( app, options ) {
  const { sender, controller } = options

  // Health check
  app.get( '/', ( req, res, next ) => {
    res.json( { status : 200, message : 'demo-ingestor alive' } )
  } )

  // Receive message
  app.post( '/recv', async ( req, res, next ) => {
    try {
      const r = controller.receiveMsg( req.body )
      res.json( r )
    } catch ( ex ) {
      next( ex )
    }
  } )
}

module.exports = {
  install
}
