const log = require( '@leverege/log' )
const cors = require( 'cors' )
const helmet = require( 'helmet' )
const express = require( 'express' )
const bodyParser = require( 'body-parser' )
const compression = require( 'compression' )


function create( options ) {

  const { corsOrigin, compressionThreshold = 1024, logRequest = true, timeRequest = false } = options 

  const app = express()

  // Enable CORS 
  const corsOptions = {
    origin( origin, cb ) {
      const good = corsOrigin === '*' || 
        ( typeof corsOrigin === 'string' && corsOrigin.indexOf( origin ) >= 0 )
      cb( good ? null : { status : 403, message : 'CORs not authorized' }, good )
    },
    credentials : true,
    allowHeaders : 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
    maxAge : 1728000, // 20 days
  }

  app.use( cors( corsOptions ) )
  app.use( helmet( ) )

  app.use( compression( { threshold : compressionThreshold } ) )
  app.use( bodyParser.json() )
  app.use( bodyParser.urlencoded( { extended : false } ) )

  // log every request
  if ( logRequest ) {
    app.use( ( req, res, next ) => {
      log.debug( { 
        method : req.method, 
        path : req.path, 
        user : req.user || null, 
        params : req.params 
      } )
      next()
    } )
  }

  // Time how long it takes to run
  if ( timeRequest ) {
    app.use( responseTime() )
  }

  return app
}

function addErrorHandler( app ) {

  app.use( ( err, req, res, next ) => {
    
    // let express handle a already started errors 
    if ( res.headersSent ) {
      next( err )
      return
    }
    const status = err.status || 500
    const rtn = { status }

    if ( err.log !== false ) { // dont log if err.log === false 
      if ( typeof err.log === 'string' ) {
        try {
          log[err.log]( { err, req } )
        } catch ( ex ) {
          log.error( { logLevel : err.log }, 'err.log was not a valid log function level. Logging under error' )
          log.error( { err, req } )
        }
      } else {
        log.error( { err, req } )
      }
    }

    rtn.message = typeof err === 'string' ? err : ( err.message || 'internal error' );

    res.status( status )
    res.json( rtn )
  } )
}

function responseTime() {
  return ( req, res, next ) => {
    const start = Date.now()
    if ( res._responseTime ) { // eslint-disable-line
      return next()
    }
    res._responseTime = true // eslint-disable-line

    res.on( 'finish', () => {
      const duration = Date.now() - start
      log.trace( { req }, `${req.method} ${req.path} in `, duration, 'ms' )
    } )

    return next()
  }
}

exports.create = create
exports.addErrorHandler = addErrorHandler
