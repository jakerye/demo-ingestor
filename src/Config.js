const Env = require( '@leverege/env' )

exports.create = ( options = process.env ) => {

  const env = Env.create( options )

  // shared config
  const transportConfig = env.json( 'TRANSPORT_CONFIG', {
    type : 'nsq',
    host : env.string( 'NSQ_HOST_ADDR', '127.0.0.1' ),
    port : env.int( 'NSQ_HOST_PORT', 4150 )
  } )
  
  return {
    serverName : env.string( 'SERVER_NAME', 'demo-ingestor' ),

    reader : {
      transportConfig,
      topic : env.string( 'MY_READER_TOPIC', 'myTopic' )
      // channel : by default, this will be myTopic-default-processor
    },
  
    writer : {
      transportConfig
    },
  
    controller : { 
      // 
    },
  
    api : {
      port : env.int( 'SERVER_PORT', 8080 ),
      corsOrigin : env.string( 'CORS_ORIGIN', '*' )
    },
  
    myProcessor : {
      processorOptions : { }
      // other options
    },
    /*
    cache : {
      namespace : 'myServer',
      cacheConfig : env.json( 'CACHE_CONFIG', {
        type : 'redis',
        connection : {
          host : '127.0.0.1',
          port : 6379
        }
      } )
    },

    lock : {
      namespace : 'myServerLock',
      lockConfig : env.json( 'LOCK_CONFIG', {
        type : 'redis',
        connection : {
          host : '127.0.0.1',
          port : 6379
        },
        lockOptions : {
          retryCount : 3, // default is 0
          retryDelay : 20, // time in ms, default is 200
          retryJitter : 4 // time in ms, default is 100
        }
      } )
    },

    limit : {
      namespace : 'myServerLimit',
      limitConfig : env.json( 'LIMIT_CONFIG', {
        type : 'redis',
        connection : {
          host : '127.0.0.1',
          port : 6379
        },
      } )
    },

    sequelizeExample : env.json( 'SQL_CONFIG', {
      host : env.string( 'SQL_HOST' || 'localhost' ),
      port : env.int( 'SQL_PORT', 3306 ),
      username : env.string( 'SQL_USER' ),
      password : env.string( 'SQL_PASSWORD' ),
      database : env.string( 'SQL_DATABASE' ),
      dialect : env.string( 'SQL_DIALECT' ),
      dialectOptions : env.json( 'SQL_DIALECT_OPTIONS', null ),
      ssl : sqlSsl,
      timezone : env.string( 'SQL_TIMEZONE', '+00:00' ),
      connectionLimit : env.int( 'SQL_POOL_CONNECTION_LIMIT', 10 ),
      pool : {
        max : env.int( 'SQL_POOL_MAX_CONNECTION', 10 ),
        min : env.int( 'SQL_POOL_MIN_CONNECTION', 1 ),
        acquire : env.int( 'SQL_POOL_ACQUIRE', 30000 ),
        idle : env.int( 'SQL_POOL_IDLE', 10000 ),
      },
      logging : false
    } )
    */
  }
}
