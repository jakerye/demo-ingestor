
require( 'dotenv/config' )
require( './InitLog' ) // Set up log configuration
const Exit = require( '@leverege/exit' )
const { Status } = require( '@leverege/message-queue' )
const ClusterManager = require( '@leverege/cluster-manager' )


// Install graceful async exit
Exit.installDefaultHandlers()

// TODO: replace this with status
Status.setDelegate( ClusterManager )

ClusterManager.start( { }, ( ) => {
  
  // Lazy load the Config and Server code
  const Config = require( './Config' )
  const MyServer = require( './MyServer' )

  MyServer.start( Config.create() )
} )
