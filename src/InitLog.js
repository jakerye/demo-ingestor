const ClusterManager = require( '@leverege/cluster-manager' )
const log = require( '@leverege/log' )
const bunyan = require( 'bunyan' )

// Get the cluster log name
const logName = ClusterManager.logName( 'demo-ingestor' )

const defaultLog = { 
  type : 'bunyan',
  name : logName,
  serializers : bunyan.stdSerializers,
  streams : [ {
    type : 'rotating-file',
    period : '1d',
    count : 3,
    level : process.env.LOG_LEVEL || 'warn',
    path : `${process.env.LOG_DIR || './logs'}/${logName}.log`
  } ]
}

const logConfig = process.env.LOG_CONFIG || defaultLog

log.createAdapter({ logConfig }, {
  logName,
  logServiceAccount : process.env.LOG_SERVICE_ACCOUNT
}, true )
  
module.exports = log
