const { Messages } = require( '@leverege/messages' )
const { MessageProcessor } = require( '@leverege/message-queue' )

class MyProcessor extends MessageProcessor {

  async process( message, msgOptions ) {
    const msgData = message.getMessage()
    const msg = Messages.create( msgData ) // make sure its valid

    // Process your message
    // if ( msg instanceof MyMsg ) {
    //   resolve with null to let the current message route onward
    //   resolve with false to stop all routing
    //   resolve with an object to send it to the route
    // } 
  }
}

module.exports = MyProcessor
