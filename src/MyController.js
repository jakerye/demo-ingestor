const log = require( '@leverege/log' )

class MyController {

  constructor( options ) {
    this.options = options
  }

  receiveMsg( msg ) {
    log.info( { inboundMsg : msg }, 'demo-ingestor inbound message received' )
  }

}

module.exports = { MyController }
