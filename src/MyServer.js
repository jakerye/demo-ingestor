const log = require( '@leverege/log' )
const Exit = require( '@leverege/exit' )
const MsqQueue = require( '@leverege/message-queue' )
const { MyController } = require( './MyController' )
const MyProcessor = require( './MyProcessor' )
const { App, Routes } = require( './api' )

const { MessageProcessor } = MsqQueue 
 
async function start( options ) {
  const { serverName } = options

  log.info( `Starting ${serverName}` )
  
  // If caching, locking, or limiting is needed
  // const cache = Cache.create( options.cache )
  // const lock = Lock.create( options.lock )
  // const limit = Limit.create( options.limit )

  // const writer = MsqQueue.createWriter( options.writer )
  // const processor = new MyProcessor( { ...options.processor, writer /* lock, cache, limit */ } )
  // const reader = MsqQueue.createReader( { ...options.reader, processor /* lock, cache, limit */ } )

  // await writer.start()
  // await reader.start()

  const controller = new MyController( { ...options.controller /* lock, cache, limit */ } )
  
  // Setup express server if needed
  const app = App.create( options.api )

  // Install routes
  Routes.install( app, { controller /* options */ } )

  // Has to be after all other route registrations
  App.addErrorHandler( app )

  // Start listening
  const apiServer = app.listen( options.api.port )

  // Handle graceful exit
  Exit.add( ( ) => { 
    return new Promise( ( resolve ) => { 
      apiServer.close( resolve ) 
    } ) 
  } )
}


module.exports = { start }
